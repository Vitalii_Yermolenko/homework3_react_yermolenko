import React from 'react'
import { useOutletContext } from 'react-router-dom'
import { ProductsList } from '../components/ProductsList';
import { PageWrapper, PageTitle } from "../style/StyleComponents.jsx";

export function Favorites() {
    const { products, favorites, setFavorites,handleAddFavoritesClick,handleAddToBasketClick,setProductChoose,productChoose,modalVisible, setModalVisible } = useOutletContext();

    const productList = products.filter(product => favorites.includes(product.id))
        .map((product) => ({
            ...product,
            isFavorite: true
        }))

    return (
        <PageWrapper>
            <PageTitle>FAVORITES</PageTitle>
            <div>
                <ProductsList
                    products={productList}
                    handleAddToFavorites={handleAddFavoritesClick}
                    handleAddToBasket={handleAddToBasketClick}
                    setProductChoose={setProductChoose}
                    productChoose={productChoose}
                    modalVisible={modalVisible}
                    setModalVisible={setModalVisible}
                />
            </div>
        </PageWrapper>
    )
}