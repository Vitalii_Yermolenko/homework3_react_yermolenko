import React, { useState } from 'react'
import { Outlet } from 'react-router-dom';
import {Header} from '../components/Header'
import { useEffect } from 'react';
import { Button } from '../components/Button';
import { Modal } from '../components/Modal';
import {ModalAgreeButtonBlock} from '../style/StyleComponents';



const savedFavorites = localStorage.getItem("favorites") === null ? [] : JSON.parse(localStorage.getItem("favorites"));
const savedBasket = localStorage.getItem("basket") === null ? {} : JSON.parse(localStorage.getItem("basket"));

export function Main() {
    const [favorites, setFavorites] = useState(savedFavorites);
    const [basket, setBasket] = useState(savedBasket)
    const [products, setProducts] = useState([]);
    const [modalVisible, setModalVisible] = useState({modalAgree: false, modalDelete:false});
    const [productChoose, setProductChoose] = useState(null);



    useEffect(() => {
        fetch("./data.json", {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json'
            }
          })
          .then(response => response.json())
          .then(data => setProducts(data))
          .catch(error => console.error(error));
    }, [])

    useEffect(() => {
        localStorage.setItem("favorites", JSON.stringify(favorites));
        
    }, [favorites])

    useEffect(() => {
        localStorage.setItem("basket", JSON.stringify(basket));
    }, [basket])

    const handleBasketSum = (basket) => {
        let sum = 0;
        Object.values(basket).forEach(value => {
            sum += value;
          });
          return sum;

    }

    const handleAddFavoritesClick = (productId) => {

        if (favorites.includes(productId)) {
            setFavorites(state => state.filter(fav => fav !== productId))
        } else {
            setFavorites(state => [...state, productId])
        }
    }

    const handleDeleteWithBasket = (productId) => {
        const { [productId]: deletedProduct, ...newBasket } = basket;
        setBasket(newBasket);
      }

    const productList = products.map(product => ({
        ...product,
        isFavorite: favorites.includes(product.id)
    }))

    const handleAddToBasketClick = (productId) => {
        console.log(productChoose);
        if (basket[productId]) {
            setBasket({
                ...basket,
                [productId]: basket[productId] + 1
            })
        } else {
            setBasket({
                ...basket,
                [productId]: 1
            })
        }
    }

    return (
        <div>
            <Header favorites={favorites}
            basket={basket}
            handleBasketSum={handleBasketSum}
            />
            
            <Outlet context={{
                products,
                favorites,
                setFavorites,
                basket,
                setBasket,
                productList,
                setProducts,
                handleAddFavoritesClick,
                handleAddToBasketClick,
                setProductChoose,
                productChoose,
                modalVisible, 
                setModalVisible,
                handleDeleteWithBasket
            }} />

            {modalVisible.modalAgree && (
            <Modal
                className='modal-buy'
                header='Good choice!'
                text={`Do you want to buy "${productChoose.name}" ?`}
                onClose={() => setModalVisible({modalAgree:false})}
                actions={
                <ModalAgreeButtonBlock className="buttons">
                    <Button
                    backgroundColor='green'
                    text='Yes!'
                    handleClick={() => {
                        handleAddToBasketClick(productChoose.id);
                        setModalVisible({modalAgree:false});
                    }}
                    />
                    <Button
                    text='No'
                    backgroundColor='red'
                    handleClick={() => {
                        setModalVisible({modalAgree:false});
                    }}
                    />
                </ModalAgreeButtonBlock>
                }
            />
            )}   


            {modalVisible.modalDelete && (
            <Modal
                className='modal-delete'
                header='Delete product with basket'
                text={`Do you want to delete "${productChoose.name}" with basket?`}
                onClose={() => setModalVisible({modalDelete:false})}
                actions={
                <ModalAgreeButtonBlock className="buttons">
                    <Button
                    backgroundColor='red'
                    text='Yes!'
                    handleClick={() => {
                        handleDeleteWithBasket(productChoose.id);
                        setModalVisible({modalDelete:false});
                    }}
                    />
                    <Button
                    text='No'
                    backgroundColor='green'
                    handleClick={() => {
                        setModalVisible({modalDelete:false});
                    }}
                    />
                </ModalAgreeButtonBlock>
                }
            />
            )}           
        </div>

    )
}