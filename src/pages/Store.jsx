import { useOutletContext } from 'react-router-dom'
import { ProductsList } from '../components/ProductsList';
import { PageWrapper, PageTitle } from "../style/StyleComponents.jsx";

export function Store() {
    const { productList,handleAddFavoritesClick, handleAddToBasketClick,setProductChoose,productChoose,modalVisible, setModalVisible} = useOutletContext();

 



    return (
        <PageWrapper>
        <PageTitle>Store</PageTitle>
        <ProductsList
            products={productList}
            handleAddToFavorites={handleAddFavoritesClick}
            handleAddToBasket={handleAddToBasketClick}
            setProductChoose={setProductChoose}
            productChoose={productChoose}
            modalVisible={modalVisible}
            setModalVisible={setModalVisible}

        />
        </PageWrapper>
    )
}