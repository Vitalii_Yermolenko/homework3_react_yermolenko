import { useOutletContext } from 'react-router-dom'
import { Product } from '../components/Product';
import { CardsWrapper, PageWrapper, PageTitle, BasketCardBlock } from "../style/StyleComponents.jsx";
import { Button } from '../components/Button';

export function Basket() {
    const { productList, basket, handleAddFavoritesClick, handleAddToBasketClick, setProductChoose, productChoose, modalVisible, setModalVisible, handleDeleteWithBasket } = useOutletContext();
    
    if(productList.length === 0){
        return null;
    }

    const basketProducts = Object.entries(basket).map(([productId, quantity]) => {
        const product = productList.find(product =>{
          return  product.id === +productId;
        });

        return { product, quantity }
    })

    return (
        <PageWrapper>
            <PageTitle>BASKET</PageTitle>
            <CardsWrapper>
                {basketProducts.map(({ product, quantity }) => {
                    
                    return (<div key={product.id}>
                        <BasketCardBlock>
                            <p>Qantity: {quantity}</p>
                            <Button
                                className="close"
                                text='X'
                                backgroundColor='grey'
                                handleClick={() => {
                                    setProductChoose(product);
                                    setModalVisible({ modalDelete: true })
                                }} />
                        </BasketCardBlock>
                        <Product
                            product={product}
                            handleAddToFavorites={handleAddFavoritesClick}
                            handleAddToBasket={handleAddToBasketClick}
                            setProductChoose={setProductChoose}
                            productChoose={productChoose}
                            modalVisible={modalVisible}
                            setModalVisible={setModalVisible}
                        />
                    </div>)
                })}
            </CardsWrapper>
        </PageWrapper>
    )
}