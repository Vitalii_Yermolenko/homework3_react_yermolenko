import React from "react";
import { Product } from "./Product";
import PropTypes from "prop-types";
import { CardsWrapper } from "../style/StyleComponents.jsx";

export function ProductsList({ products, handleAddToBasket, handleAddToFavorites,setProductChoose,productChoose,modalVisible, setModalVisible }) {

  if(!products){
    return(
      <div>No products</div>
    )
  }

  return (
    <CardsWrapper>
      {products.map((product) => (
        <Product
          key={product.id}
          product={product}
          handleAddToBasket={handleAddToBasket}
          handleAddToFavorites={handleAddToFavorites}
          setProductChoose={setProductChoose}
          productChoose={productChoose}
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
        />
      ))}
      </CardsWrapper>
  );
}

ProductsList.propTypes = {
  products: PropTypes.array,
  handleAddToBasket: PropTypes.func,
  handleAddToFavorites: PropTypes.func,
}
